package mision.tic.Elecciones_Colegio.repositorio;

import mision.tic.Elecciones_Colegio.modelo.Representantes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioRepresentantes extends JpaRepository<Representantes,Integer> {

}
