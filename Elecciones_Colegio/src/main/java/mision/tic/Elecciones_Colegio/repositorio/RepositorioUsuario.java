package mision.tic.Elecciones_Colegio.repositorio;


import mision.tic.Elecciones_Colegio.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioUsuario extends JpaRepository<Usuario,Integer> {

    boolean existsByCorreo(String correo);
    Usuario findByCorreo(String correo);

}
