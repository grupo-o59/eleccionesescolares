package mision.tic.Elecciones_Colegio.repositorio;

import mision.tic.Elecciones_Colegio.modelo.Usuario;
import mision.tic.Elecciones_Colegio.modelo.Votos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioVotos extends JpaRepository<Votos,Integer> {
    boolean existsByUsuario(Usuario usuario);
}
