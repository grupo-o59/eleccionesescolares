package mision.tic.Elecciones_Colegio.controlador;

import lombok.Getter;
import mision.tic.Elecciones_Colegio.Entradas.EntradaLogin;
import mision.tic.Elecciones_Colegio.Entradas.EntradaUsuario;
import mision.tic.Elecciones_Colegio.modelo.Usuario;
import mision.tic.Elecciones_Colegio.salidas.SalidaGeneral;
import mision.tic.Elecciones_Colegio.servicio.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api")
public class ControladorUsuario {

    @Autowired
    ServicioUsuario servicioUsuario;

    @GetMapping("/obtenerUsuario")
    public ResponseEntity<Object> obtenerUsuario(@RequestParam String correo){

        Usuario usuario = servicioUsuario.obtenerUsuario(correo);
        if(usuario != null){
            return ResponseEntity.status(HttpStatus.OK)
                    .body(
                            SalidaGeneral.builder()
                                    .codigo(200)
                                    .mensaje("Este es el usuario")
                                    .respuesta(usuario)
                                    .fecha(new Date())
                                    .build()
                    );
        }else{
            return ResponseEntity.status(HttpStatus.OK)
                    .body(
                            SalidaGeneral.builder()
                                    .codigo(200)
                                    .mensaje("Ese usuario no existe")
                                    .respuesta(null)
                                    .fecha(new Date())
                                    .build()
                    );
        }

    }


    @PostMapping("/login")
    public ResponseEntity<Object> iniciarSesion(@RequestBody EntradaLogin entradaLogin){
        int rta = servicioUsuario.iniciarSesion(entradaLogin);
        switch (rta){
            case 0:
                return ResponseEntity.status(HttpStatus.OK)
                    .body(
                            SalidaGeneral.builder()
                                    .codigo(200)
                                    .mensaje("Inicio exitoso")
                                    .respuesta(true)
                                    .fecha(new Date())
                                    .build()
                    );
            case 1:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("Contraseña no coincide")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
            default:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("Correo no existe")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
        }
    }

    @PostMapping("/agregarUsuario")
    public ResponseEntity<Object> agregarUsuario(@RequestBody EntradaUsuario usuario){
        if(servicioUsuario.crearUsuario(usuario)){
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(
                            SalidaGeneral.builder()
                                    .codigo(201)
                                    .mensaje("Se creo el usuario")
                                    .respuesta(true)
                                    .fecha(new Date())
                                    .build()
                    );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(
                        SalidaGeneral.builder()
                                .codigo(404)
                                .mensaje("No se logro crear el usuario")
                                .respuesta(false)
                                .fecha(new Date())
                                .build()
                );


    }

    @DeleteMapping("/eliminarUsuario")
    public ResponseEntity<Object> eliminarUsuario(@RequestBody EntradaLogin entradaLogin){
        int rta = servicioUsuario.eliminarUsuario(entradaLogin);
        switch (rta){
            case 0:
                return ResponseEntity.status(HttpStatus.OK)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(200)
                                        .mensaje("Elimino el usuario")
                                        .respuesta(true)
                                        .fecha(new Date())
                                        .build()
                        );
            case 1:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("Contraseña no coincide")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
            default:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("Correo no existe")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
        }
    }
}
