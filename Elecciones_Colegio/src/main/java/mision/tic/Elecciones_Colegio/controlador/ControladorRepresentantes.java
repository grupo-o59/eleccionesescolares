package mision.tic.Elecciones_Colegio.controlador;

import mision.tic.Elecciones_Colegio.Entradas.EntradaRepresentantes;
import mision.tic.Elecciones_Colegio.modelo.Representantes;
import mision.tic.Elecciones_Colegio.salidas.SalidaGeneral;
import mision.tic.Elecciones_Colegio.servicio.ServicioRepresentantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ControladorRepresentantes {

    @Autowired
    ServicioRepresentantes servicioRepresentantes;

    @GetMapping("/todosRepresentantes")
    public ResponseEntity<Object> todosRepresentantes(){
        List<Representantes> representantes = servicioRepresentantes.todosRepresentantes();
        return ResponseEntity.status(HttpStatus.OK)
                .body(
                        SalidaGeneral.builder()
                                .codigo(200)
                                .mensaje("Estos son todos los representantes")
                                .respuesta(representantes)
                                .fecha(new Date())
                                .build()
                );
    }

    @GetMapping("/representantePorId")
    public ResponseEntity<Object> representantePorId(@RequestParam int id){
        Representantes representante = servicioRepresentantes.representantePorId(id);
        if(representante != null){
            return ResponseEntity.status(HttpStatus.OK)
                    .body(
                            SalidaGeneral.builder()
                                    .codigo(200)
                                    .mensaje("Este es el representante con id "+id)
                                    .respuesta(representante)
                                    .fecha(new Date())
                                    .build()
                    );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(
                        SalidaGeneral.builder()
                                .codigo(404)
                                .mensaje("Ese estudiante no existe")
                                .respuesta(null)
                                .fecha(new Date())
                                .build()
                );

    }

    @PostMapping("/crearRepresentante")
    public ResponseEntity<Object> crearRepresentante(@RequestBody EntradaRepresentantes entradaRepresentantes){
        boolean rta = servicioRepresentantes.crearRepresentante(entradaRepresentantes);
        if(rta){
            return ResponseEntity.status(HttpStatus.OK)
                    .body(
                            SalidaGeneral.builder()
                                    .codigo(200)
                                    .mensaje("Se creo el representante")
                                    .respuesta(true)
                                    .fecha(new Date())
                                    .build()
                    );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(
                        SalidaGeneral.builder()
                                .codigo(404)
                                .mensaje("No es posible crear el representante")
                                .respuesta(false)
                                .fecha(new Date())
                                .build()
                );
    }

}
