package mision.tic.Elecciones_Colegio.controlador;

import mision.tic.Elecciones_Colegio.salidas.SalidaGeneral;
import mision.tic.Elecciones_Colegio.servicio.ServicioVotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api")
public class ControladorVotos {
    @Autowired
    ServicioVotos servicioVotos;

    @PostMapping("/votar")
    public ResponseEntity<Object> votar(@RequestParam int id_Usuario,@RequestParam int id_Representante){
        int rta = servicioVotos.votar(id_Usuario,id_Representante);
        switch (rta){
            case 0:
                return ResponseEntity.status(HttpStatus.OK)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(200)
                                        .mensaje("Se realizo el voto")
                                        .respuesta(true)
                                        .fecha(new Date())
                                        .build()
                        );
            case 1:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("El estudiante ya voto")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
            case 2:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("El representante no existe")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
            default:
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(
                                SalidaGeneral.builder()
                                        .codigo(406)
                                        .mensaje("El usuario no existe")
                                        .respuesta(false)
                                        .fecha(new Date())
                                        .build()
                        );
        }
    }
}
