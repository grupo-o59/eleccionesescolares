package mision.tic.Elecciones_Colegio.servicio;

import mision.tic.Elecciones_Colegio.modelo.Representantes;
import mision.tic.Elecciones_Colegio.modelo.Usuario;
import mision.tic.Elecciones_Colegio.modelo.Votos;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioRepresentantes;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioUsuario;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioVotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServicioVotos {
    @Autowired
    RepositorioVotos repositorioVotos;

    @Autowired
    RepositorioUsuario repositorioUsuario;

    @Autowired
    RepositorioRepresentantes repositorioRepresentantes;

    public int votar(int id_Usuario, int id_Representante){
        if(repositorioUsuario.existsById(id_Usuario)){
            if(repositorioRepresentantes.existsById(id_Representante)){
                Usuario usuario = repositorioUsuario.findById(id_Usuario).get();
                if(!repositorioVotos.existsByUsuario(usuario)){
                    Representantes representante = repositorioRepresentantes.findById(id_Representante).get();
                    Votos votos = new Votos(usuario,representante);
                    repositorioVotos.save(votos);
                    return 0;
                }else{
                    return 1;//El estudiante ya voto
                }
            }else{
                return 2;//El representante no existe
            }
        }else{
            return 3;//El usuario no existe
        }
    }
}
