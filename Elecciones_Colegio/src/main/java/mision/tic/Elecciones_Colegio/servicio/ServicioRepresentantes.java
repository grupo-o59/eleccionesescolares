package mision.tic.Elecciones_Colegio.servicio;

import lombok.extern.java.Log;
import mision.tic.Elecciones_Colegio.Entradas.EntradaRepresentantes;
import mision.tic.Elecciones_Colegio.modelo.Representantes;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioRepresentantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioRepresentantes {
    @Autowired
    RepositorioRepresentantes repositorioRepresentantes;
    public List<Representantes> todosRepresentantes() {
        return repositorioRepresentantes.findAll();
    }

    public Representantes representantePorId(int id) {
        if(repositorioRepresentantes.existsById(id)){
            return repositorioRepresentantes.findById(id).get();
        }
        return null;
    }

    public boolean crearRepresentante(EntradaRepresentantes entradaRepresentantes) {
        try{
            Representantes representantes = new Representantes(
                    entradaRepresentantes.getNombre(),
                    entradaRepresentantes.getApellido(),
                    entradaRepresentantes.getCurso());
            repositorioRepresentantes.save(representantes);
            return true;
        }catch (Exception e){
            System.err.println(e.toString());
            return false;
        }
    }


}
