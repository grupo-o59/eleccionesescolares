package mision.tic.Elecciones_Colegio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EleccionesColegioApplication {

	public static void main(String[] args) {
		SpringApplication.run(EleccionesColegioApplication.class, args);
	}

}
