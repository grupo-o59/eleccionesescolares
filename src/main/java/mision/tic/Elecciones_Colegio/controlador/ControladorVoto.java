package mision.tic.Elecciones_Colegio.controlador;

import mision.tic.Elecciones_Colegio.servicio.ServicioVotar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ControladorVoto {

    @Autowired
    ServicioVotar servicioVotar;

    @GetMapping("/votar/")
    public void votar(@RequestParam int usuario_id,@RequestParam int representante_id){
        servicioVotar.votar(usuario_id,representante_id);
    }

}
