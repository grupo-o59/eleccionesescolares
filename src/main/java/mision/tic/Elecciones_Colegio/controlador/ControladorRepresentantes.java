package mision.tic.Elecciones_Colegio.controlador;

import mision.tic.Elecciones_Colegio.Entradas.EntradaRepresentate;
import mision.tic.Elecciones_Colegio.modelo.Representantes;
import mision.tic.Elecciones_Colegio.salidas.SalidaGeneral;
import mision.tic.Elecciones_Colegio.servicio.ServicioRepresentantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class ControladorRepresentantes {

    @Autowired
    ServicioRepresentantes servicioRepresentantes;

    @GetMapping("/todosrepresentantes")
    public ResponseEntity<Object> todosrepresentantes(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(
                        SalidaGeneral.builder()
                                .codigo(200)
                                .mensaje("Estos son todos los representantes")
                                .respuesta(servicioRepresentantes.todosrepresentantes())
                                .fecha(new Date())
                                .build()
                );
    }

    @GetMapping("/obtenerrepresentanteporid")
    public ResponseEntity<Object> obtenerrepresentanteporid(@RequestParam int id){
        String mensaje = "";
        Representantes representante =  servicioRepresentantes.obtenerrepresentanteporid(id);
        if(representante!=null){
            mensaje = "Este es el representante con id = "+id;
        }else{
            mensaje = "El representante con id = "+id+" no existe";
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(
                        SalidaGeneral.builder()
                                .codigo(200)
                                .mensaje(mensaje)
                                .respuesta(representante)
                                .fecha(new Date())
                                .build()
                );
    }


    @PostMapping("/crearrepresentante")
    public ResponseEntity<Object> crearrepresentante(@RequestBody EntradaRepresentate entradaRepresentate){
        String mensaje = "";
        short codigo;
        boolean respuesta = servicioRepresentantes.crearrepresentante(entradaRepresentate);
        if(respuesta){
            codigo = (short) 200;
            mensaje = "Se creo el representante";
        }else{
            codigo = (short) 404;
            mensaje = "No es posible crear el representante";
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(
                        SalidaGeneral.builder()
                                .codigo(codigo)
                                .mensaje(mensaje)
                                .respuesta(respuesta)
                                .fecha(new Date())
                                .build()
                );
    }

}
