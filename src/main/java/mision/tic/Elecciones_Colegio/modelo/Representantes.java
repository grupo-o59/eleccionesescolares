package mision.tic.Elecciones_Colegio.modelo;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data //getter setter toString
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor con todos los argumentos
@RequiredArgsConstructor
@Entity
public class Representantes {
    @Id //columna como id primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto incremental
    private int id;
    @NonNull
    private String nombre;
    @NonNull
    private String apellido;
    @NonNull
    private String grupo;

}
