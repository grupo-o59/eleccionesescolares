package mision.tic.Elecciones_Colegio.modelo;


import lombok.*;

import javax.persistence.*;

@Data //getter setter toString
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor con todos los argumentos
@RequiredArgsConstructor
@Entity
public class Votos {
    @Id //columna como id primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto incremental
    private int id;

    @NonNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="usuario_id",referencedColumnName = "id",nullable = false,unique = true)
    private Usuario usuario;
    @NonNull
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "representantes_id",referencedColumnName = "id",nullable = false)
    private Representantes representantes;



}
