package mision.tic.Elecciones_Colegio.modelo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data //getter setter toString
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor con todos los argumentos
@Entity
public class Estudiantes {
    @Id //columna como id primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto incremental
    private int id;
    private String nombre;
    private String apellido;
    private byte edad;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "estudiantes_profesores",
               joinColumns = @JoinColumn(name = "fk_estudiantes" , nullable = false,unique = true),
               inverseJoinColumns = @JoinColumn(name = "fk_profesores", nullable = false)
               )
    private List<Profesores> profesores;

}
