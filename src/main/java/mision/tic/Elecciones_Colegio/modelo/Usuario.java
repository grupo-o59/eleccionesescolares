package mision.tic.Elecciones_Colegio.modelo;


import lombok.*;

import javax.persistence.*;

@Data //getter setter toString
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor con todos los argumentos
@RequiredArgsConstructor
@Entity
public class Usuario {
    @Id //columna como id primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto incremental
    private int id;
    @NonNull
    private String nombreUsuario;
    @NonNull
    @Column(name = "correo",unique = true)
    private String correo;
    @NonNull
    private String contraseña;
    @NonNull
    private boolean estado;


}
