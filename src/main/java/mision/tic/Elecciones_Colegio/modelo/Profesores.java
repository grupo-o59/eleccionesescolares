package mision.tic.Elecciones_Colegio.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data //getter setter toString
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor con todos los argumentos
@Entity
public class Profesores {
    @Id //columna como id primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto incremental
    private int id;
    private String nombre;
    private String apellido;
    private byte edad;
    @ManyToMany(mappedBy = "profesores")
    private List<Estudiantes> estudiantes;

}
