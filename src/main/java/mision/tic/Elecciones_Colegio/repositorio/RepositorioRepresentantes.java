package mision.tic.Elecciones_Colegio.repositorio;


import mision.tic.Elecciones_Colegio.modelo.Representantes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface RepositorioRepresentantes extends JpaRepository<Representantes,Integer> {

}
