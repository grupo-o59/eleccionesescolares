package mision.tic.Elecciones_Colegio.Entradas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //getter setter toString
@NoArgsConstructor //Constructor sin argumentos
@AllArgsConstructor //Constructor con todos los argumentos
public class EntradaLogin {
    private String correo;
    private String contraseña;
}
