package mision.tic.Elecciones_Colegio.servicio;

import mision.tic.Elecciones_Colegio.Entradas.EntradaRepresentate;
import mision.tic.Elecciones_Colegio.modelo.Representantes;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioRepresentantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public class ServicioRepresentantes {
    @Autowired
    RepositorioRepresentantes repositorioRepresentantes;
    public List<Representantes> todosrepresentantes() {
        return repositorioRepresentantes.findAll();
    }

    public Representantes obtenerrepresentanteporid(int id){
        if(repositorioRepresentantes.existsById(id)){
            return repositorioRepresentantes.findById(id).get();
        }
        return null;
    }

    public boolean crearrepresentante(EntradaRepresentate entradaRepresentate) {
        Representantes representante = new Representantes(entradaRepresentate.getNombre(),entradaRepresentate.getApellido(), entradaRepresentate.getGrupo());
        try {
            repositorioRepresentantes.save(representante);
            return true;
        }catch (Exception e){
            System.err.println(e.toString());
            return false;
        }

    }
}
