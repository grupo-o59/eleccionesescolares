package mision.tic.Elecciones_Colegio.servicio;

import mision.tic.Elecciones_Colegio.Entradas.EntradaLogin;
import mision.tic.Elecciones_Colegio.Entradas.EntradaUsuario;
import mision.tic.Elecciones_Colegio.modelo.Usuario;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServicioUsuario {

    @Autowired
    RepositorioUsuario repositorioUsuario;

    public boolean crearUsuario(EntradaUsuario entradaUsuario){

        if(entradaUsuario.getContraseña1().equals(entradaUsuario.getContraseña2())
                && entradaUsuario.getCorreo().contains("@")
                && entradaUsuario.getCorreo().contains(".")){
            Usuario usuario = new Usuario(entradaUsuario.getNombreUsuario(),
                    entradaUsuario.getCorreo(),
                    entradaUsuario.getContraseña1(),
                    true);
            repositorioUsuario.save(usuario);
            return true;
        }
        return false;


    }

    public int iniciarSesion(EntradaLogin entradaLogin){
        if(repositorioUsuario.existsByCorreo(entradaLogin.getCorreo())){
            Usuario usuario = repositorioUsuario.findByCorreo(entradaLogin.getCorreo());
            if(entradaLogin.getContraseña().equals(usuario.getContraseña())){
                return 0; // Se logro inicio exito
            }
            return 1; //Fallo por contraseña correo si existe
        }
        return 2; //Falla por correo no existe
    }


    public int eliminarUsuario(EntradaLogin entradaLogin) {
        if(repositorioUsuario.existsByCorreo(entradaLogin.getCorreo())){
            Usuario usuario = repositorioUsuario.findByCorreo(entradaLogin.getCorreo());
            if(entradaLogin.getContraseña().equals(usuario.getContraseña())){
                repositorioUsuario.delete(usuario);
                return 0;
            }
            return 1;
        }
        return 2;
    }
}
