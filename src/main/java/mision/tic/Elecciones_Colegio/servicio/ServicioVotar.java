package mision.tic.Elecciones_Colegio.servicio;

import mision.tic.Elecciones_Colegio.modelo.Representantes;
import mision.tic.Elecciones_Colegio.modelo.Usuario;
import mision.tic.Elecciones_Colegio.modelo.Votos;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioRepresentantes;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioUsuario;
import mision.tic.Elecciones_Colegio.repositorio.RepositorioVotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServicioVotar {

    @Autowired
    RepositorioVotos repositorioVotos;

    @Autowired
    RepositorioRepresentantes repositorioRepresentantes;

    @Autowired
    RepositorioUsuario repositorioUsuario;

    public void votar(int usuario_id,int representante_id) {
        if(repositorioUsuario.existsById(usuario_id) && repositorioRepresentantes.existsById(representante_id)){
            Usuario usuario = repositorioUsuario.findById(usuario_id).get();
            Representantes representante = repositorioRepresentantes.findById(representante_id).get();
            Votos voto = new Votos(usuario,representante);
            repositorioVotos.save(voto);
        }


    }
}
